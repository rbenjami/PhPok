<?php

require_once( __DIR__ . '/../../controller.inc.php' );
require_once( __DIR__ . '/../../srcs/views/mimetypes/JsonView.php' );
require_once( __DIR__ . '/../../srcs/views/templatings/TwigView.php' );

class Test implements JsonSerializable
{
	private $_pok;

	public function __construct( $pok )
	{
		$this->_pok = $pok;
	}

	/**
	 * @inheritdoc
	 */
	function jsonSerialize()
	{
		return json_encode([
			'pok' => $this->_pok,
			'poky' => array(1, 2, "3" => (object)"pok")
		]);
	}
}

class HomeController extends Controller
{
	/**
	 * @return bool
	 */
	public static function index()
	{
//		JsonView::getInstance()->render( new Test( "pok" ) );
//		TwigView::setPaths( __DIR__ . '/../templates/', __DIR__ . '/../templates/caches' );
		$view = new TwigView( 'home.html' );
		$view->render( array('navigation' =>
			array(
				'item 1' => array(
					'href' => '/',
					'caption' => 'Item 1'
				),
				'item2' => array(
					'href' => '/not_found',
					'caption' => 'Item 2'
				)
			), 'a_variable' => 'A variable') );
		return true;
	}
}