<?php

/**
 * Simple demo site
 */
require_once( __DIR__ . '/../router.inc.php' );

Router::loadFromJsonFile(__DIR__ . "/config/routes.json");

Router::match( Router::getInstance() );