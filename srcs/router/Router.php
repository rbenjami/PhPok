<?php

require_once( __DIR__ . '/../../third_party/AltoRouter.php' );
require_once( __DIR__ . '/Method.php' );
require_once( __DIR__ . '/../views/View.php' );
require_once( __DIR__ . "/../configLoader/JsonConfigLoader.php" );

/**
 * Class Router use AltoRouter third party
 *
 * @see AltoRouter
 * @see http://altorouter.com/
 */
class Router
{
	/**
	 * Map of AltoRouter instances
	 *
	 * @var array
	 */
	private static $_instances;

	private static $controllers = array();

	/**
	 * Get instance by host
	 *
	 * @param string $host
	 * @return AltoRouter
	 */
	public static function getInstance( $host = '' )
	{
		if ( Router::$_instances[$host] == null )
			Router::$_instances[$host] = new AltoRouter( array(), $host );
		return Router::$_instances[$host];
	}

	/**
	 * Get all instances
	 *
	 * @return array
	 */
	public static function getInstances()
	{
		return Router::$_instances;
	}

	/**
	 * Load the specified config file with JSONConfigLoader and
	 * call loadFromArray with the result
	 */
	public static function loadFromJsonFile( $jsonFile, AltoRouter $router = null )
	{
		$jsonFileLoader = new JSONConfigLoader( $jsonFile );
		$jsonFileLoader->loadFile();
		$jsonConfig = $jsonFileLoader->getConfigArray();

		if ( $jsonConfig )
			self::loadFromArray( $jsonConfig );
	}

	/**
	 * Take an array, check the formating and store the controller path in self::controllers
	 */
	public static function loadFromArray( array $routesDefined, AltoRouter $router = null )
	{
		if ( $router == null )
			$router = self::getInstance();

		foreach ( $routesDefined as $uri => $details )
		{
			foreach ($details as $method => $infos)
			{
				if ( !isset( $infos["controller"] ) || !isset( $infos["function"] )
				|| !isset( $infos["controllerPath"] ) )
				{
					if ( !isset( $infos["function"] ) )
						trigger_error("Error with your route config at URI : " . $uri . " method : " . $method );
					continue;
				}
				if ( !file_exists( $infos["controllerPath"] ) )
				{
					trigger_error("Your specify an incorrect controllerPath " . $infos["controllerPath"] . " in your route config. This entry was ignored URI : $uri , method : $method" );
					continue;
				}
				$router->map( $method , $uri, [ $infos["controller"], $infos["function"] ] );
				self::$controllers[ md5( $infos["controller"] . $infos["function"] ) ] = $infos["controllerPath"];
			}
		}
	}

	/**
	 * @param AltoRouter $router
	 * @return bool
	 */
	public static function match( AltoRouter $router )
	{
		// match current request url
		$match = $router->match();
		// call closure or throw 404 status
		if ( $match && is_callable( $match['target'] ) )
		{
			call_user_func_array( $match['target'], $match['params'] );
			return true;
		}
		else if ( $match ) // route found but isn't callable, try to include
		{
			$key = implode( "", $match['target'] );
			$key = md5( $key );
			if ( isset( self::$controllers[$key] ) )
			{
				include_once( self::$controllers[$key] );
				if ( is_callable( $match['target'] ) )
				{
					call_user_func_array( $match['target'], $match['params'] );
					return true;
				}
				else
					self::notFound();
			}
		}
		 // no route was matched
		self::notFound();
		return false;
	}

	private static function notFound()
	{
		$SERVER_PROTOCOL = "HTTP/1.0";
		if ( isset( $_SERVER["SERVER_PROTOCOL"] ) )
			$SERVER_PROTOCOL = $_SERVER["SERVER_PROTOCOL"];
		header( $SERVER_PROTOCOL . ' 404 Not Found', true, 404 );
	}
}