<?php

/**
 * Class Method
 *
 * The set of common methods for HTTP/1.1 is defined below and this set can be expanded based on requirements.
 * These method names are case sensitive and they must be used in uppercase.
 */
abstract class Method
{
	/**
	 * Requests data from a specified resource
	 *
	 * @var string
	 */
	const GET = "GET";

	/**
	 * Same as GET but returns only HTTP headers and no document body
	 *
	 * @var string
	 */
	const HEAD = "HEAD";

	/**
	 * Submits data to be processed to a specified resource
	 *
	 * @var string
	 */
	const POST = "POST";

	/**
	 * Uploads a representation of the specified URI
	 *
	 * @var string
	 */
	const PUT = "PUT";

	/**
	 * Same as PUT
	 *
	 * @depreciate
	 * @var string
	 */
	const PATCH = "PATCH";

	/**
	 * Deletes the specified resource
	 *
	 * @var string
	 */
	const DELETE = "DELETE";

	/**
	 * Returns the HTTP methods that the server supports
	 *
	 * @var string
	 */
	const OPTIONS = "OPTIONS";

	/**
	 * Converts the request connection to a transparent TCP/IP tunnel
	 *
	 * @var string
	 */
	const CONNECT = "CONNECT";

	/**
	 * Performs a message loop-back test along the path to the target resource.
	 *
	 * @var string
	 */
	const TRACE = "TRACE";
}