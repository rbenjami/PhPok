<?php

require_once( __DIR__ . '/../View.php' );
require_once( __DIR__ . '/../../../vendor/autoload.php' );

class TwigView implements View
{
	/**
	 * @var Twig_Environment
	 */
	private static $_twig = null;

	/**
	 * @var Twig_Loader_Filesystem
	 */
	private static $_loader = null;

	/**
	 * @var string
	 */
	private static $_templatesPath = null;

	/**
	 * @var string
	 */
	private static $_cachePath = null;

	private $_template;

	/**
	 * TwigView constructor.
	 * @param string $template
	 */
	public function __construct( $template )
	{
		$this->_template = TwigView::getTwig()->loadTemplate( $template );
	}

	/**
	 * @param mixed $data
	 * @param bool $print
	 *
	 * @return string
	 */
	public function render( $data, $print = true )
	{
		if ( $this->_template != null )
			$printable = $this->_template->render( $data );
		if ( $print )
			print $printable;
		return $printable;
	}

	/**
	 * @param $templatePath
	 * @param null $cachePath
	 * @return Twig_Environment
	 */
	public static function setPaths( $templatePath, $cachePath = null )
	{
		TwigView::$_templatesPath = $templatePath;
		TwigView::$_cachePath = $cachePath;

		TwigView::$_loader = new Twig_Loader_Filesystem( $templatePath );
		TwigView::$_twig = new Twig_Environment( TwigView::$_loader, array(
			'cache' => $cachePath,
		) );
		return TwigView::$_twig;
	}

	/**
	 * @return Twig_Environment
	 */
	public static function getTwig()
	{
		if ( self::$_twig == null )
			self::setPaths( 'templates/', 'templates/caches/' );
		return self::$_twig;
	}
}