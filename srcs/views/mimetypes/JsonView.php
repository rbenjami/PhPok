<?php

require_once( __DIR__ . '/../View.php' );

class JsonView implements View
{
	private static $_instance;

	/**
	 * @return JsonView
	 */
	public static function getInstance()
	{
		if ( JsonView::$_instance == null )
			JsonView::$_instance = new JsonView();
		return JsonView::$_instance;
	}

	/**
	 * @param $data JsonSerializable
	 * @param $print bool
	 *
	 * @return string
	 */
	public function render( $data, $print = true )
	{
		if ( $data == null )
			$printable = "null";
		else
			$printable = json_encode( $data->jsonSerialize() );
		if ( $print )
			print $printable;
		return $printable;
	}
}