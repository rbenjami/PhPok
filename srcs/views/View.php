<?php

interface View
{
	/**
	 * @param $data mixed
	 * @param $print bool
	 * 
	 * @return string
	 */
	public function render( $data, $print = true );
}
