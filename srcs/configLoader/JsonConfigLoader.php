<?php

require_once( __DIR__ . "/AConfigLoader.php" );


/**
 * Class JSONConfigLoader extends from the abstract class AConfigLoader
 *
 * Loader for json config file
 */
class JSONConfigLoader extends AConfigLoader
{

	/**
	 * call AConfigLoader constructor
	 * and define json errors define if doesn't exist (PHP < 5.5.0)
	 *
	 * @see AConfigLoader
	 */
	function __construct( $filePath )
	{
		parent::__construct( $filePath );
		$this->_path = $filePath;

		if ( !defined( "JSON_ERROR_RECURSION" ) )
			define( "JSON_ERROR_RECURSION", null );
		if ( !defined( "JSON_ERROR_INF_OR_NAN" ) )
			define( "JSON_ERROR_INF_OR_NAN", null );
		if ( !defined( "JSON_ERROR_UNSUPPORTED_TYPE" ) )
			define( "JSON_ERROR_UNSUPPORTED_TYPE", null );
	}

	/**
	 * Load the specified config file and store in _configArray
	 * if an error occured, it send a E_USER_ERROR
	 */
	public function loadFile()
	{
		$str = file_get_contents( $this->_path );
		if ( $str === false )
			return ;

		$json = json_decode( $str, true );
		if ( $json == null )
		{
			$errorMsg = "An error was occured with your json config file ($this->_path) : ";
			$errorMsg .= PHP_EOL . $this->getJSONErrorString();
			trigger_error($errorMsg , E_USER_ERROR);
			return ;
		}
		$this->_configArray = $json;
	}

	/**
	 * Return $_configArray
	 */
	public function getConfigArray()
	{
		return ( $this->_configArray );
	}

	/**
	 * Get the error message for the last json error
	 * call this function after a json_decode if it return null
	 *
	 * @return string
	 */
	private function getJSONErrorString()
	{
		switch ( json_last_error() ) {
			case JSON_ERROR_NONE:
				return ( "No error has occurred" );
			break;
			case JSON_ERROR_DEPTH:
				return ( "The maximum stack depth has been exceeded" );
			break;
			case JSON_ERROR_STATE_MISMATCH:
				return ( "Invalid or malformed JSON" );
			break;
			case JSON_ERROR_CTRL_CHAR:
				return ( "Control character error, possibly incorrectly encoded" );
			break;
			case JSON_ERROR_SYNTAX:
				return ( "Syntax error" );
			break;
			case JSON_ERROR_UTF8:
				return ( "Malformed UTF-8 characters, possibly incorrectly encoded" );
			break;
			case JSON_ERROR_RECURSION:
				return ( "One or more recursive references in the value to be encoded" );
			break;
			case JSON_ERROR_INF_OR_NAN:
				return ( "One or more NAN or INF values in the value to be encoded" );
			break;
			case JSON_ERROR_UNSUPPORTED_TYPE:
				return ( "A value of a type that cannot be encoded was given" );
			break;
			default:
				return ( "Unknow error" );
			break;
		}
	}
}