<?php

/**
 * Abstract class AConfigLoader
 *
 * The parent class for load, parse and translate config files to php digest array
 */
abstract class AConfigLoader
{
	/**
	 * Absolute config file path
	 *
	 * @var string
	 */
	protected $_path;

	/**
	 * Digest translation of config file in array
	 *
	 * @var array
	 */
	protected $_configArray;

	/**
	 * Construct save config file path and send
	 * an E_USER_ERROR if it doesn't exist
	 *
	 * @param string $filePath
	 */
	function __construct( $filePath )
	{
		if ( !file_exists( $filePath ) )
		{
			trigger_error("File :" . $filePath . " doesn't exist" , E_USER_ERROR);
			$_path = null;
		}
		else
			$_path = $filePath;
		$_configArray = null;
	}

	/**
	 * Load the specified config file and store in _configArray
	 */
	abstract public function loadFile();

	/**
	 * Return $_configArray
	 */
	abstract public function getConfigArray();

}