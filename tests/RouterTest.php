<?php

require_once( __DIR__ . '/../router.inc.php' );

class RouterTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @var AltoRouter
	 */
	protected $_router;

	protected function setUp()
	{
		$this->_router = Router::getInstance();
	}

	/**
	 * @covers Router::getInstance
	 */
	public function testGetInstance()
	{
		$this->assertArrayHasKey( '', Router::getInstances() );
	}

	/**
	 * @covers Router::match
	 * @runInSeparateProcess
	 */
	public function testMatch()
	{
		$this->_router->map( Method::GET, '/test', function(){} );

		$_SERVER['REQUEST_URI'] = '/test';
		$this->assertTrue( Router::match( $this->_router ) );

		$_SERVER['REQUEST_URI'] = '/failed_test';
		$this->assertFalse( Router::match( $this->_router ) );
	}
}