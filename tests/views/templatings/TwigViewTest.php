<?php

//require_once( __DIR__ . '/../../../srcs/views/templatings/TwigView.php' );
//
///**
// * Class TwigViewTest
// *
// * @covers TwigView
// */
//class TwigViewTest extends PHPUnit_Framework_TestCase
//{
//	/**
//	 * @covers TwigView::setPaths
//	 */
//	public function testSetPaths()
//	{
//		TwigView::setPaths( __DIR__ . '/templates/', __DIR__ . '/templates/caches' );
//	}
//
//	/**
//	 * @covers TwigView::render
//	 */
//	public function testRender()
//	{
//		$view = new TwigView( 'test.html' );
//		$response = $view->render( array('navigation' =>
//			array(
//				'item 1' => array(
//					'href' => '/',
//					'caption' => 'Item 1'
//				),
//				'item2' => array(
//					'href' => '/not_found',
//					'caption' => 'Item 2'
//				)
//			), 'a_variable' => 'A variable'), false );
//		$this->assertEquals( "<!DOCTYPE html>
//<html>
//<head>
//	<title>My Webpage</title>
//</head>
//<body>
//<ul id=\"navigation\">
//		<li><a href=\"/\">Item 1</a></li>
//		<li><a href=\"/not_found\">Item 2</a></li>
//	</ul>
//
//<h1>My Webpage</h1>
//A variable
//</body>
//</html>
//", $response );
//	}
//}
