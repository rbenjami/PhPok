<?php

require_once( __DIR__ . '/../../../srcs/views/mimetypes/JsonView.php' );

class Test implements JsonSerializable
{
	private $_test;

	public function __construct( $test )
	{
		$this->_test = $test;
	}

	/**
	 * @inheritdoc
	 */
	function jsonSerialize()
	{
		return [
			'test' => $this->_test,
			'test2' => array(1, 2, "3" => (object)"test")
		];
	}
}

/**
 * Class JsonViewTest
 * @covers JsonView
 */
class JsonViewTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers JsonView::render
	 */
	public function testRender()
	{
		$test = new Test( "JsonView test" );
		$json_object = json_encode( $test->jsonSerialize() );

		$response = JsonView::getInstance()->render( $test, false );
		$this->assertJsonStringEqualsJsonString( $json_object, $response );

		$response = JsonView::getInstance()->render( null, true );
		$this->assertEquals( "null", $response );
	}
}
