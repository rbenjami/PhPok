### Status

[![build status](https://gitlab.com/rbenjami/PhPok/badges/master/build.svg)](https://gitlab.com/rbenjami/PhPok/commits/master)
[![Dependency Status](https://gemnasium.com/badges/3cb894c3852e365182bed18972632cc0.svg)](https://gemnasium.com/1d635461d40f0996f1d573cd45a68fa0)

### PhPok

PhPok is a lightweight library/framework for building a API or/and WebSite
